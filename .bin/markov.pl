#!/usr/bin/perl
use strict;
use warnings;
use Algorithm::MarkovChain;

my $file = shift @ARGV;
my $tag = shift @ARGV;
my $chain = make_chain($file, $tag);

# Chain things that follow an end-of-sentence marker
my @spew = $chain->spew(complete => [ '$PARA' ], length => 200);

# trim off our starting symbol and cut the spew down to a complete sentence
shift @spew;
pop @spew while $spew[-1] !~ m/[\?!\.]/;

print clean(@spew), "\n";

# Get the string of words
sub make_chain
{
    my $file = shift;
    my $chain = Algorithm::MarkovChain->new();

    open my $fh, "<", $file or die "Couldn't open $file: $!";
    my @tokens = ('$PARA');
    while (defined (my $line = <$fh>))
    {
        chomp $line;
        if ($line)
        {
            push @tokens, split_tokens($line);
        }
        else
        {
            push @tokens, '$PARA';
        }
    }
    $chain->seed(symbols => \@tokens);
    return $chain;
}

sub split_tokens
{
    my @words = ();
    $_ = shift;
    chomp;
    if ($_)
    {
        my @chunk = grep { defined and length } split /[\s<>"{}()\[\]]+/;
        while (@chunk)
        {
            my $word = shift @chunk;
            if ($word =~ m/(\w+)([,\.!\?-])+$/)
            {
                push @words, $1, $2;
            }
            else
            {
                push @words, $word;
            }
        }
    }

    return @words;
}

sub clean
{
    $_ = join " ", @_;
    s/\s*([\.\?!,:;])/$1/g;
    s/\s*(['\-])\s*/$1/g;
    s/\$PARA/\n\n/g;
    return $_;
}
