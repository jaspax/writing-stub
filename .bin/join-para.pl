#!/usr/bin/perl

use strict;
use warnings;

my $doc = join '', <>;
$doc =~ s/(\S)\n(\S)/$1 $2/mg;
print $doc;
