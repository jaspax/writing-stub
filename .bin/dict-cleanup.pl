#!/usr/bin/perl
use strict;
use warnings;

my ($doc, $dict) = @ARGV;
my (%words_in_dict, %words_in_doc);

if (not $doc or not $dict) {
    print "Usage: dict-cleanup.pl [document.mkd] [dictionary.spl]\n";
    exit;
}

{
    open my $fh, "<", $dict or die "Couldn't open $dict: $!";
    %words_in_dict = map { chomp; $_ => 0; } <$fh>;
}

{
    open my $fh, "<", $doc or die "Couldn't open $doc: $!";
    %words_in_doc = map { chomp; $_ => 0; } map { split /[\s,\.\?!"\-]+/ } <$fh>;
}

my @found = grep { exists $words_in_doc{$_} } keys %words_in_dict;

{
    open my $fh, ">", $dict, or die "Couldn't open $dict for writing: $!";
    print $fh "$_\n" foreach sort @found;
}
