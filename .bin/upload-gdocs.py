# Help i'm writing my first python script

import sys
import os
import httplib2
from oauth2client import client
from oauth2client import file
from apiclient import discovery
from apiclient import errors

def auth():
    "log in with credentials and store them locally"

    store_file = os.environ["WRITING_ROOT"] + "/.bin/upload-gdocs-credentials"
    store = file.Storage(store_file)
    credentials = store.get()

    if (not credentials):
        json_file = os.environ["WRITING_ROOT"] + "/.bin/upload-gdocs-secret.json"
        flow = client.flow_from_clientsecrets(json_file, scope="https://www.googleapis.com/auth/drive", redirect_uri="urn:ietf:wg:oauth:2.0:oob")

        auth_uri = flow.step1_get_authorize_url()
        print "Go to the following URL in your web browser and login\n"
        print auth_uri + "\n"

        auth_code = raw_input('Then enter the auth code here: ')

        credentials = flow.step2_exchange(auth_code)
        store.put(credentials)

    return credentials

def get_drive(cred):
    http = cred.authorize(httplib2.Http())
    drive = discovery.build('drive', 'v2', http)
    return drive

def find_writing(files):
    writing_list = files.list(fields="items(id,title)", q="title = 'Writing'").execute()
    items = writing_list["items"]
    if (len(items) != 1):
        raise "Found %d folders named '%s', expecting 1" % (len(items), "Writing")
    return items[0]

def find_folder(files, writing_folder, name):
    query = "'%s' in parents and title = '%s'" % (writing_folder["id"], name)
    folder_list = files.list(fields="items(id,title)", q=query).execute()
    items = folder_list["items"]
    if (len(items) > 1):
        raise "Found %d folders named '%s', expecting 1" % (len(items), name)
    if (len(items)):
        return items[0]
    return None

def make_folder(files, parent, name):
    body = {'title': name, 'parents': [{'id': parent["id"]}], 'mimeType': "application/vnd.google-apps.folder"}
    folder = files.insert(body=body).execute()
    return folder

def insert_file(files, parent, name, path):
    body = {'title': name, 'parents': [{'id': parent["id"]}]}
    fdesc = files.insert(body=body, convert=False, media_body=path).execute()
    return fdesc

def find_file(files, parent, name):
    query = "'%s' in parents and title = '%s'" % (parent["id"], name)
    file_list = files.list(q=query, fields="items(id,title)").execute()
    items = file_list["items"]
    if (len(items) > 1):
        raise "Found %d files named '%s', expecting 1" % (len(items), name)
    if (len(items)):
        return items[0];
    return None

def update_file(files, existing_fdesc, path):
    f = files.update(fileId=existing_fdesc["id"], media_body=path, convert=False).execute()
    return f

files = get_drive(auth()).files()
writing = find_writing(files)

name = sys.argv[1]
folder = find_folder(files, writing, name)
if (folder is None):
    folder = make_folder(files, writing, name)

for path in sys.argv[2:]:
    basename = os.path.basename(path)
    f = find_file(files, folder, basename)
    if (f is None):
        f = insert_file(files, folder, basename, path)
        print "Added %s" % basename
    else:
        f = update_file(files, f, path)
        print "Updated %s" % basename

