#!/usr/bin/perl
use strict;
use warnings;

package strgen;

sub gen(\%\%$)
{
    my %map = %{ shift @_ };
    my %pattern = %{ shift @_ };
    my $count = shift;
    my @rv = ();

    my $total = 0;
    $total += $_ foreach values %pattern;

    for (1 .. $count)
    {
        # select a pattern
        my $patval = int(rand($total));
        my $pattern = "";
        keys %pattern; # reset hash iter
        while (my ($pat, $val) = each %pattern)
        {
            if ($val > $patval)
            {
                $pattern = $pat;
                last;
            }
            $patval -= $val;
        }

        my $string = "";
        foreach my $class (split "", $pattern)
        {
            $string .= $map{$class}[ int(rand(scalar @{$map{$class}})) ];
        }

        push @rv, $string;
    }

    return @rv;
}

1;
