#!/usr/bin/perl
use strict;
use warnings;

my @naughty = qw{
    fuck
    fucked
    fucking
    fucks
    damn
    damned
    shit
    shits
    dick
    dicks
    cunt
    cunts
    tit
    tits
    bitch
    bitches
};

my $fname = shift @ARGV;
open my $fh, "<", $fname or die "Couldn't open $fname for reading: $!";
open my $out, ">", "clean-$fname" or die "Couldn't open clean-$fname for writing: $!";

foreach my $line (<$fh>) {
    foreach (@naughty) {
        $line =~ s/\b$_\b/bunny/g;
    }
    print $out $line;
}
