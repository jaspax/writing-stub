from os import path
from wikitools import wiki
from wikitools.page import BadTitle, NoPage, Page
from dateutil.parser import parse
from dateutil.tz import tzlocal
import re
import sys
from datetime import datetime

if len(sys.argv) < 2:
    raise "Must invoke with at least one file argument"

site = wiki.Wiki("http://jsbangs.conlang.org/api.php");
site.login("jaspax", "iY#PhlevvPp7fZ4K");

for fname in sys.argv[1:]:
    title = path.basename(fname)
    title = re.sub("(\.zout)?\.wiki$", "", title)
    title = re.sub("--", ":", title)

    try:
        page = Page(site, title)
        history = page.getHistory(content=False, limit=1)
        stamp = parse(history[0]['timestamp'])
        if stamp >= datetime.fromtimestamp(path.getmtime(fname), tzlocal()):
            print("{} has been modified more recently on the server; skipping".format(title))
            continue

    except NoPage:
        print("Page '{}' doesn't exist yet".format(title))
        pass # Page doesn't exist yet; ignore this since we'll create it below

    print("Updating '{}'... ".format(title))
    with open(fname, "r") as myfile:
        txt = myfile.read()
        page.edit(text=txt, summary="Updated from markdown source", minor=False, bot=True, skipmd5=True)
        print("Done with {}.".format(title))


