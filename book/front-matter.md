# Copyright and Thanks

<div class="small blocks">

_Book Title_

Copyright © 2016 by J.S. Bangs.

Join my mailing list and get my newsletter filled with upcoming releases, sales, and other
news. [Click here to sign up.](http://goo.gl/UjAvWN)
            
Other stuff here, probably.

This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International
License. You may find a summary of the license and a link to the full license here:
[http://creativecommons.org/licenses/by-nc/4.0/](http://creativecommons.org/licenses/by-nc/4.0/)

</div>
