# About Me

![](../promo-materials/headshot.png){#headshot}\

<div class="blocks">

Hi, I'm J.S. Bangs. I live in Romania with my family of four, where I work as a freelance
software developer, make cheese, play video games, and write books. If you're interested in
getting announcements about future releases, contests, and giveaways, [sign up for my
mailing list](http://goo.gl/OEqgRx) or visit the following link in your web browser:

[http://goo.gl/UjAvWN](http://goo.gl/OEqgRx)

You can also get in touch with me at any of the following places:

Website: [jsbangs.com](http://jsbangs.com)

Twitter: [\@jsbpax](https://twitter.com/jsbpax)

Facebook: [https://www.facebook.com/jsbangswrites](https://www.facebook.com/jsbangswrites)

If you liked this book, you can leave a review at any of the following places:

[Amazon](http://www.amazon.com/J.S.-Bangs/e/B00GZ8VZZA)

[Goodreads](https://www.goodreads.com/author/show/4460539.J_S_Bangs)

</div>
