<!-- vim: fo= 
-->

# Docs on how we use our link shortener

Our favorite shortener: https://goo.gl

The target page is http://jsbangs.conlang.org/signup-form.html. We can use two different
query strings to distinguish both source and target:

    f=[from]

This is where the link is coming *from*. We use a two or three-letter abbreviation to
indicate the book, site, advertising medium that targets this link.

    s=[segment]

This indicates the segment that the link should add subscribers to. A little JS on the
signup-page.html will handle the magic to make this retarget the correct list segment.  If
this doesn't exist, then we target the default segment.

# Known source abbreviations

We use these with f= in order to distinguish the place that the link is coming from.

Abbreviation    Source 
------------    ------
jsb             jsbangs.com
sbe             Storm Bride ebook
sbp             Storm Bride paperback
wse             Wave Speaker ebook

# Known segment abbreviations

Segment         Usage
-------         -----
(empty)         The default signup. These are "organic" signups that reach the page from non-paid links.
fb              Facebook advertising, default. (Specific campaigns may have other hooks)
