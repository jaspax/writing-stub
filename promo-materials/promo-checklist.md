# One month before publishing

Send the book to reviewers.

Send a "sneak peak" email to mailing list, notifying them of the pub date.

Put the cover image up at jsbangs.com, with release date

Buffer tweets about upcoming release

# Publish

Attach cover to ebook

Generate epubs and mobi files

Upload cover to Amazon

Upload interior to Amazon

Hit "publish"

# Immediately after publishing

Get the ASIN after the book appears on Amazon.

Add the book on Goodreads with title, ASIN, cover copy, etc. Make sure that it appears in
order.

Update previous book in the series and change the mailing list link to a "buy now" link.
Republish that book.

Update the book you JUST PUBLISHED and change the generic "review" links (Amazon and
Goodreads) to a live link specific for this book. Republish that book.

Send a message to the mailing list announcing that the book is out. Notify them that there's
a $0.99 sale thru the weekend.

Schedule a mailchimp message for 10-14 days after release. Solicit reviews and remind people
they can buy it.

Buffer tweets about new release

## Update links

Go to goo.gl in order to create new shortlinks. Use the correct f= tag in order to
distinguish sources. Update link-shortener.md with the new f parameters that you've used.

## Creating a new mailing list segment

Decide on a new segment code and add it to link-shortener.md 

Create new links in goo.gl in order to track the success of the segment.
