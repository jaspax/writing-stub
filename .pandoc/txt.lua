-- These are common functions used by all of the Lua writers

function read_address()
    local fh = assert(io.open("../address.txt"))
    local txt = fh:read("*all")
    fh:close()
    return txt
end

function flatten_variables(metadata, variables)
    local rv = {}

    -- merge metadata with variables
    for k, v in pairs(metadata) do
        rv[k] = v
    end

    -- flatten the nested tables in variables
    for k, v in pairs(variables) do
        if type(v) == 'table' then
            for innerK, innerV in pairs(v) do
                rv[innerK] = innerV
            end
        else
            rv[k] = v
        end
    end

    return rv
end

-- This function is called once for the whole document. Parameters:
-- body is a string, metadata is a table, variables is a table.
-- One could use some kind of templating
-- system here; this just gives you a simple standalone HTML file.
function Doc(body, metadata, variables)
    local buffer = {}
    local function add(s)
      table.insert(buffer, s)
    end

    variables = flatten_variables(metadata, variables)

    assert(variables['author'], "no author name provided")
    assert(variables['title'], "no title provided")
    assert(variables['lastname'], "no last name provided")

    -- address
    add(CodeBlock(variables['author'] .. "\n" .. read_address()))

    -- title and author
    add("\n" .. metadata['title']:upper() .. "\nby " .. variables['author'])

    -- wordcount
    if variables['wordcount'] then
        add(variables['wordcount'] .. " words\n")
    end

    add(body)

    return table.concat(buffer, "\n")
end

-- Blocksep is used to separate block elements.
function Blocksep()
  return "\n\n"
end

-- The functions that follow render corresponding pandoc elements.
-- s is always a string, attr is always a table of attributes, and
-- items is always an array of strings (the items in a list).
-- Comments indicate the types of other variables.

function Str(s)
  return s
end

function Space()
  return " "
end

function LineBreak()
  return "\n"
end

function Emph(s)
  return "_" .. s .. "_"
end

function Strong(s)
  return "**" .. s .. "**"
end

function Subscript(s)
  return "_" .. s
end

function Superscript(s)
  return "^" .. s
end

function SmallCaps(s)
  return s
end

function Strikeout(s)
  return s
end

-- TODO: unimplemented
-- function Link(s, src, tit)
--   return "<a href='" .. escape(src,true) .. "' title='" ..
--          escape(tit,true) .. "'>" .. s .. "</a>"
-- end

-- TODO: unimplemented
-- function Image(s, src, tit)
--  return "<img src='" .. escape(src,true) .. "' title='" ..
--          escape(tit,true) .. "'/>"
-- end

-- TODO: unimplemented
-- function Code(s, attr)
--   return "<code" .. attributes(attr) .. ">" .. escape(s) .. "</code>"
-- end

-- TODO: unimplemented
-- function InlineMath(s)
--   return "\\(" .. escape(s) .. "\\)"
-- end

-- TODO: unimplemented
-- function DisplayMath(s)
--   return "\\[" .. escape(s) .. "\\]"
-- end

-- TODO: unimplemented
-- function Note(s)
-- end

function Span(s, attr)
    return Plain(s)
end

function Plain(s)
  return s
end

function Para(s)
  return s
end

-- lev is an integer, the header level. s is a string, attr the attributes
-- note that we INVERT the normal section hierarchy here for convenience
-- level 1 = chapter
-- level 2 = part
local chapterNumber = 0
local partNumber = 0
function Header(lev, s, attr)
    if lev == 1 then -- chapters
        chapterNumber = chapterNumber + 1
        return Para("CHAPTER " .. chapterNumber .. ": " .. s:upper())
    elseif lev == 2 then -- part
        partNumber = partNumber + 1
        return Para("PART " .. partNumber .. ": " .. s:upper())
    else
        io.stderr:write("UNSUPPORTED header level " .. lev)
    end
end

function BlockQuote(s)
    return "     " .. s
end

-- we use horizontal rules for scene breaks, and render them as such
function HorizontalRule()
    return Para("     #")
end

function CodeBlock(s, attr)
    return s
end

-- TODO: unimplemented
-- function BulletList(items)
--   local buffer = {}
--   for _, item in pairs(items) do
--     table.insert(buffer, "<li>" .. item .. "</li>")
--   end
--   return "<ul>\n" .. table.concat(buffer, "\n") .. "\n</ul>"
-- end

-- TODO: unimplemented
-- function OrderedList(items)
--   local buffer = {}
--   for _, item in pairs(items) do
--     table.insert(buffer, "<li>" .. item .. "</li>")
--   end
--   return "<ol>\n" .. table.concat(buffer, "\n") .. "\n</ol>"
-- end

-- TODO: unimplemented
-- Revisit association list STackValue instance.
-- function DefinitionList(items)
--   local buffer = {}
--   for _,item in pairs(items) do
--     for k, v in pairs(item) do
--       table.insert(buffer,"<dt>" .. k .. "</dt>\n<dd>" ..
--                         table.concat(v,"</dd>\n<dd>") .. "</dd>")
--     end
--   end
--   return "<dl>\n" .. table.concat(buffer, "\n") .. "\n</dl>"
-- end

-- TODO: unimplemented
-- Caption is a string, aligns is an array of strings,
-- widths is an array of floats, headers is an array of
-- strings, rows is an array of arrays of strings.
-- function Table(caption, aligns, widths, headers, rows)
-- end

function Div(s, attr)
  return Para(s)
end

-- Block comments (don't render)
function RawBlock(s)
    return ""
end

-- Inline comments (don't render)
function RawInline(s)
    return ""
end

-- The following code will produce runtime warnings when you haven't defined
-- all of the functions you need for the custom writer, so it's useful
-- to include when you're working on a writer.
local meta = {}
meta.__index =
  function(_, key)
    io.stderr:write(string.format("WARNING: Undefined function '%s'\n",key))
    return function() return "" end
  end
setmetatable(_G, meta)

function main()
    Doc("test", {}, {})
end
