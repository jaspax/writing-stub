-- These are common functions used by all of the Lua writers

function read_address()
    local fh = assert(io.open("../address.txt"))
    local txt = fh:read("*all")
    fh:close()
    return txt
end

function flatten_variables(metadata, variables)
    local rv = {}

    -- merge metadata with variables
    for k, v in pairs(metadata) do
        rv[k] = v
    end

    -- flatten the nested tables in variables
    for k, v in pairs(variables) do
        if type(v) == 'table' then
            for innerK, innerV in pairs(v) do
                rv[innerK] = innerV
            end
        else
            rv[k] = v
        end
    end

    return rv
end

-- Character escaping
local function escape(s, in_attribute)
    local replacements = {
        ["#"] = "\\#",
        ["$"] = "\\$",
        ["%"] = "\\%",
        ["&"] = "\\&",
        ["~"] = "\\~",
        ["_"] = "\\_",
        ["^"] = "\\^",
        ["\\"] = "\\\\",
        ["{"] = "\\{",
        ["}"] = "\\}",
        ["\n"] = "\\\\\n"
    }

    return s:gsub(".",
        function(x)
            if replacements[x] then
                return replacements[x]
            end
            return x
        end)
end

-- Soft break is for breaks within the source document itself
function SoftBreak()
    return "\n"
end

-- Blocksep is used to separate block elements.
function Blocksep()
  return "\n"
end

-- This function is called once for the whole document. Parameters:
-- body is a string, metadata is a table, variables is a table.
-- One could use some kind of templating
-- system here; this just gives you a simple standalone HTML file.
function Doc(body, metadata, variables)
    local buffer = {}
    local function add(s)
      table.insert(buffer, s)
    end

    variables = flatten_variables(metadata, variables)

    assert(variables['author'], "no author name provided")
    assert(variables['title'], "no title provided")
    assert(variables['lastname'], "no last name provided")

    if variables['target'] == 'memoir' then
        add("\\documentclass[ebook,12pt,twoside,openright,final]{memoir}\n")
        add("\\usepackage[T1]{fontenc}\n\\usepackage[utf8]{inputenc}\n\\usepackage{textcomp}\n")
        add("\n% You are expected to change this to something nice\n");
        add("\\newcommand{\\newscene}{\\hrule}\n")
    else
        add("\\documentclass[" .. variables['story-type'] .. ",courier]{sffms}")
        add("\\usepackage[T1]{fontenc}\n\\usepackage[utf8]{inputenc}\n\\usepackage{textcomp}\n")

        add("\\author{" .. variables['author'] .. "}")
        add("\\surname{" .. variables['lastname'] .. "}")
        add("\\address{" .. escape(read_address()) .. "}")
        add("\\title{" .. variables['title'] .. "}")

        if variables['running-title'] then
            add("\\runningtitle{" .. variables['running-title'] .. "}")
        end
        if variables['wordcount'] then
            add("\\wordcount{" .. variables['wordcount'] .. "}")
        end
        add("\\disposable")
    end

    add("\\begin{document}")
    add(body)
    add("\\end{document}")

    return table.concat(buffer, "\n")
end

-- The functions that follow render corresponding pandoc elements.
-- s is always a string, attr is always a table of attributes, and
-- items is always an array of strings (the items in a list).
-- Comments indicate the types of other variables.

function Str(s)
  return escape(s)
end

function Space()
  return " "
end

function LineBreak()
  return "\\line"
end

function Emph(s)
  return "\\emph{" .. s .. "}"
end

function Strong(s)
  return "\\uwave{" .. s .. "}"
end

function Subscript(s)
  return "_{" .. s .. "}"
end

function Superscript(s)
  return "^{" .. s .. "}"
end

function SmallCaps(s)
  return '\\textsc{' .. s .. '}'
end

function Strikeout(s)
  return '\\sout{' .. s .. '}'
end

-- TODO: unimplemented
-- function Link(s, src, tit)
--   return "<a href='" .. escape(src,true) .. "' title='" ..
--          escape(tit,true) .. "'>" .. s .. "</a>"
-- end

-- TODO: unimplemented
-- function Image(s, src, tit)
--  return "<img src='" .. escape(src,true) .. "' title='" ..
--          escape(tit,true) .. "'/>"
-- end

-- TODO: unimplemented
-- function Code(s, attr)
--   return "<code" .. attributes(attr) .. ">" .. escape(s) .. "</code>"
-- end

-- TODO: unimplemented
-- function InlineMath(s)
--   return "\\(" .. escape(s) .. "\\)"
-- end

-- TODO: unimplemented
-- function DisplayMath(s)
--   return "\\[" .. escape(s) .. "\\]"
-- end

-- TODO: unimplemented
-- function Note(s)
-- end

function Span(s, attr)
    return Plain(s)
end

function Plain(s)
  return s
end

function Para(s)
  return s .. "\n"
end

-- lev is an integer, the header level. s is a string, attr the attributes
-- note that we INVERT the normal section hierarchy here for convenience
-- level 1 = chapter
-- level 2 = part
function Header(lev, s, attr)
    prologueTag  = "Prologue: "
    epilogueTag = "Epilogue: "

    if lev == 1 then -- chapters
        if (string.sub(s, 1, string.len(prologueTag)) == prologueTag) then
            return "\\chapter*{" .. s .. "}"
        elseif (string.sub(s, 1, string.len(epilogueTag)) == epilogueTag) then
            return "\\chapter*{" .. s .. "}"
        else
            return "\\chapter{" .. s .. "}"
        end
    elseif lev == 2 then -- part
        return "\\part{" .. s .. "}"
    else
        io.stderr:write("UNSUPPORTED header level " .. lev)
    end
end

function DoubleQuoted(s)
    return "``" .. s .. "''"
end

function SingleQuoted(s)
    return "`" .. s .. "'"
end

function BlockQuote(s)
    return "\\begin{quote}\n" .. s .. "\\end{quote}\n"
end

-- we use horizontal rules for scene breaks, and render them as such
function HorizontalRule()
    return "\\newscene"
end

function CodeBlock(s, attr)
    return "\\begin{verbatim}\n" .. s .. "\\end{verbatim}\n"
end

-- TODO: unimplemented
-- function BulletList(items)
--   local buffer = {}
--   for _, item in pairs(items) do
--     table.insert(buffer, "<li>" .. item .. "</li>")
--   end
--   return "<ul>\n" .. table.concat(buffer, "\n") .. "\n</ul>"
-- end

-- TODO: unimplemented
-- function OrderedList(items)
--   local buffer = {}
--   for _, item in pairs(items) do
--     table.insert(buffer, "<li>" .. item .. "</li>")
--   end
--   return "<ol>\n" .. table.concat(buffer, "\n") .. "\n</ol>"
-- end

-- TODO: unimplemented
-- Revisit association list StackValue instance.
-- function DefinitionList(items)
--   local buffer = {}
--   for _,item in pairs(items) do
--     for k, v in pairs(item) do
--       table.insert(buffer,"<dt>" .. k .. "</dt>\n<dd>" ..
--                         table.concat(v,"</dd>\n<dd>") .. "</dd>")
--     end
--   end
--   return "<dl>\n" .. table.concat(buffer, "\n") .. "\n</dl>"
-- end

-- TODO: unimplemented
-- Caption is a string, aligns is an array of strings,
-- widths is an array of floats, headers is an array of
-- strings, rows is an array of arrays of strings.
-- function Table(caption, aligns, widths, headers, rows)
-- end

function Div(s, attr)
  return Para(s)
end

-- Block comments (don't render)
function RawBlock(s)
    return ""
end

-- Inline comments (don't render)
function RawInline(s)
    return ""
end

-- The following code will produce runtime warnings when you haven't defined
-- all of the functions you need for the custom writer, so it's useful
-- to include when you're working on a writer.
local meta = {}
meta.__index =
  function(_, key)
    io.stderr:write(string.format("WARNING: Undefined function '%s'\n",key))
    return function() return "" end
  end
setmetatable(_G, meta)

