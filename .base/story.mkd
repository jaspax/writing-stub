% Title

## Dan Wells' 7-Point Story Outline

# Hook

This is the character's initial situation. It should be opposed to the character's final
situation along the axis which will be the character's arc. It should also give us a clue
about the characters existing plans and motivations.

# Turn 1

This is the first plot turn. Conflict is introduced; there is a call to adventure; the
character is introduced to new ideas. The status quo which held in the Hook is overturned.

# Pinch 1

Apply pressure to the character. Force the character to act. Bring the character into
conflict with the villain. Introduce a new element which further complicates the Turn.

# Midpoint

Character moves from reaction to action. Typically, the pressure from the first Pinch is
relieved, but in such a way as to commit the character to further action. At this point the
character is no longer a passive sufferer but an agent opposing the forces arrayed against
him.

# Pinch 2

The jaws of defeat. The characters goals are almost totally thwarted and he has to act
desperately in order to salvage his purposes. This frequently the loss of a mentor, a love
interest, or the loss of everything.

# Turn 2 

Victory is snatched from the jaws of defeat. The character gets the last piece which is
needed to achieve the goal, and is able to complete the arc which began in the first turn.

# Resolution

The character has been transformed, and his final state exists in contrast with his state in
the Hook. The arc is complete.
