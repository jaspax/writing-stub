.PHONY: all tex pdf html rtf clean freq wiki upload

# Include any additional .mk files in this derectory (can be used to override
# things below)
-include *.mk

# Variables related to the project itself. These variables are based on
# reasonable defaults, but can be overriden by the project-specific makefile

WRITING_ROOT?=~/writing
PROJECT?=$(shell basename $(PWD))
AUTHOR?=J.S. Bangs
LASTNAME?=Bangs
STORY_TYPE?=
WORDCOUNT?=$(call wc, $<)
SHORT_TITLE?=$(basename $(notdir $<))
RUNNING_TITLE?=$(call uc, $(SHORT_TITLE))
PROJECT_FILES=$(wildcard *.mkd)
EPUB_FILES?=meta.yaml $(PROJECT).mkd

VARIABLES=-V story-type=$(STORY_TYPE) -V "running-title=$(RUNNING_TITLE)" -V wordcount=$(WORDCOUNT) -V "author=$(AUTHOR)" -V lastname=$(LASTNAME)

pdf: $(patsubst %.mkd,out/%.pdf,$(PROJECT_FILES))

tex: $(patsubst %.mkd,out/%.tex,$(PROJECT_FILES))

paperback: $(patsubst %.mkd,out/%.paperback.pdf,$(PROJECT_FILES))

epub: $(patsubst %.mkd,out/%.epub,$(PROJECT_FILES))

html: $(patsubst %.mkd,out/%.html,$(PROJECT_FILES))

rtf: $(patsubst %.mkd,out/%.rtf,$(PROJECT_FILES))

txt: $(patsubst %.mkd,out/%.txt,$(PROJECT_FILES))

zip: $(patsubst %.mkd,out/%.zip,$(PROJECT_FILES))

upload: $(patsubst %.mkd,out/%.upload,$(PROJECT_FILES))

wiki: out/.wiki
	
out/.wiki: $(patsubst %.wiki,out/%.wiki,$(wildcard *.wiki))
	python $(WRITING_ROOT)/.bin/wiki.py $?
	touch out/.wiki

out/%.rtf: %.mkd $(WRITING_ROOT)/.pandoc/template.rtf $(WRITING_ROOT)/.pandoc/rtf.lua
	pandoc -f markdown -t $(WRITING_ROOT)/.pandoc/rtf.lua $(VARIABLES) -o $@ $<

out/%.html: %.mkd
	pandoc -f markdown -t html $(VARIABLES) -o $@ $<

out/%.tex: %.mkd $(WRITING_ROOT)/.pandoc/template.tex $(WRITING_ROOT)/.pandoc/tex.lua
	pandoc -f markdown -t $(WRITING_ROOT)/.pandoc/tex.lua $(VARIABLES) -V target=sffms -o $@ $<

# Note that the paperback.tex file is NOT put in the out/ folder, because we
# expect that we will modify it manually after generation
%.paperback.tex: %.mkd $(WRITING_ROOT)/.pandoc/tex.lua
	if [ ! -f $@ ]; then pandoc -f markdown -t $(WRITING_ROOT)/.pandoc/tex.lua $(VARIABLES) -V target=memoir -S -o $@ $<; fi

out/%.paperback.pdf: %.paperback.tex
	xelatex -output-directory out $<

out/%.pdf: out/%.tex
	TEXINPUTS="$(WRITING_ROOT)/.sffms:" xelatex -output-directory out $<

out/%.epub: $(EPUB_FILES) epub/*
	pandoc -f markdown -t epub3 --standalone --chapters --toc --smart -o $@ $(EPUB_FILES)
	$(WRITING_ROOT)/.bin/KindleGen/kindlegen -c0 -verbose $@

out/%.mobi: out/%.epub

out/%.txt: %.mkd
	pandoc -f markdown -t $(WRITING_ROOT)/.pandoc/txt.lua $(VARIABLES) -o $@ $<

out/%.zip: out/%.rtf out/%.pdf out/%.txt 
	zip $@ $^

out/%.wiki: %.wiki
	pandoc -f markdown -t MediaWiki -o $@ $<

out/%.upload: out/%.rtf out/%.pdf out/%.txt out/%.epub out/%.mobi
	python $(WRITING_ROOT)/.bin/upload-gdocs.py $(PROJECT) $^
	touch $@

clean:
	rm -rf out/*

all: clean pdf html rtf

# 
# Special handling of certain named files goes here
#

# SYNOPSIS

SYNOPSIS=synopsis.mkd
SYNOPSIS_WORDCOUNT=$(call wc, $(SYNOPSIS))
SYNOPSIS_TITLE=$(call uc, "$(PROJECT) SYNOPSIS")
SYNOPSIS_VARIABLES=-V "running-title=$(SYNOPSIS_TITLE)" -V wordcount=$(SYNOPSIS_WORDCOUNT) -V "author=$(AUTHOR)"

out/synopsis.tex: $(SYNOPSIS) $(WRITING_ROOT)/.pandoc/template.tex
	pandoc -f markdown -t latex --standalone --template=$(WRITING_ROOT)/.pandoc/template.tex $(SYNOPSIS_VARIABLES) -o $@ $<

# QUERY

QUERY=query.mkd
QUERY_VARIABLES=-V "address=$(QUERY_ADDRESS)" -V "salutation=$(QUERY_SALUTATION)" 

out/query.tex: $(QUERY) $(WRITING_ROOT)/.pandoc/query-template.tex
	pandoc -f markdown -t latex --standalone --template=$(WRITING_ROOT)/.pandoc/query-template.tex $(QUERY_VARIABLES) -o $@ $<

# 
# Functions
#

uc=$(shell echo $(1) | tr a-z A-Z)
wc=$(shell cat $(1) | wc -w)
