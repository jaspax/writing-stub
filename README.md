# Introduction

You can fork this repo to create a base for future work writing with Git, Markdown, and
Pandoc. The general setup for an individual story is described below.

# Setup

These are debian packages you are likely to need when startup up with a new machine:

    git                         to sync the enlistment itself
    pandoc                      pandoc for conversions between formats
    texlive-xetex               basic tex tools
    texlive-fonts-recommended   fonts for tex
    texlive-fonts-extra         fonts for tex
    cpan                        to be able to update Perl packages (used for a few older scripts)
    pip                         to be able to update Python packages (used for all newer scripts)

We require pandoc version >= 1.16. If the package mgr doesn't have that version yet, do the
following

    apt-get install cabal-install
    cabal update
    cabal --global install pandoc

Perl modules (not necessary):

    Algorithm::MarkovChain

Python modules (only used if uploading to google docs):

    google-api-python-client

# Adding new short stories

Short stories are generally easier than books. The directory `story/` has an example: a
simple Markdown document in a file called `story.mkd`, and a Makefile. 

When creating a new story, you can copy the contents of `story` to a new directory, then
change the name of the `.mkd` file. The name of the file should match the name of the
directory, ie. `new-story/new-story.mkd`.

The Makefile has two variables in it which should be modified by hand:

    STORY_TYPE=story 
    RUNNING_TITLE=STORY

The variable `STORY_TYPE` can be `story` or `book`, and it changes some of the targets in
the Makefile. The variable `RUNNING_TITLE` is used to display the running title along the
top of the story, and is usually a one or two word abbreviation of the full title.

The makefile can be invoked with the following targets:

    make rtf
    make pdf
    make html
    make txt

# Adding new novels

An example of a novel is in the directory `book`. It has the same `book.mkd` and `Makefile`
that the story has, but there are number of other files used to create the other parts of
the book. I find it useful to keep those separate from the main body of the story in order
to avoid distracting myself, and to get a wordcount which doesn't count any of that stuff.

The Makefile has the same two variables in it which should be modified by hand:

    STORY_TYPE=novel 
    RUNNING_TITLE=BOOK

The variable `STORY_TYPE` can be `story` or `book`, and it changes some of the targets in
the Makefile. The variable `RUNNING_TITLE` is used to display the running title along the
top of the story, and is usually a one or two word abbreviation of the full title.

Additionally, there is this variable: 

    EPUB_FILES=meta.yaml front-matter.md book.mkd other-books.md about-me.md

This is used to establish all of the contents used to build the epub. The files are added to
the epub in the order given. You can add or remove files as required.

In addition to the targets used for stories, you can build an epub and a mobi with:

    make epub

This target actually builds an epub *and* a mobi file.

# Other targets

You can look around and find things in the Makefile and the `.bin` directory which might be
useful.
